<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();

        return view('companies.index', ['companies' => $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('companies.create', ['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // print_r($request->input());
        // print_r(Auth::check());
        if (Auth::check()) {
          $company = Company::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'user_id' => Auth::user()->id
          ]);
        }

        if ($company) {
          // $projects = $company->projects;
          return redirect()->route('companies.show', [
            'company' => $company,
            // 'projects' => $projcets
          ])->with('success', 'Company created successfully!');
        }

        return back()->withInput()->with('error', 'Error creating new company');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        $company = Company::find($company->id);
        // $company = Company::where('id', $company->id)->first();
        $projects = $company->projects;

        return view('companies.show', [
          'company' => $company,
          'projects' => $projects
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        $user = $company->user;
        $role = $user->role;

        return view('companies.edit', [
          'company' => $company,
          'user' => $user,
          'role' => $role
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $companyUpdate = Company::where('id', $company->id)
                          ->update([
                            'name' => $request->input('name'),
                            'description' => $request->input('description')
                          ]);


        if ($companyUpdate) {
          return redirect()->route('companies.show', ['company' => $company->id])
                           ->with('success', 'Company updated successfully');
        }
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $companyToDelete = Company::find($company->id);

        if ($companyToDelete->delete()) {
          // redirect
          return redirect()->route('companies.index')
          ->with('success', 'Company Deleted Successfully');
        }

        return back()->with('error', 'Company could not be deleted');

    }
}
