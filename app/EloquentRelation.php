<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


/**
* One to One
*/
class User extends Model
{
  public function phone()
  {
    return $this->hasOne('App\Phone');
  }
}

// Define the inverse of the Relationship
class Phone extends Model
{
  public function phone()
  {
    return $this->belongsTo('App\Phone');
  }
}

/**
* One to Many
*/
class Post extends Model
{
  public function comments()
  {
    return $this->hasMany('App\Comment');
  }
}

// Define the inverse of the Relationship
class Comment extends Model
{
  public function post()
  {
    return $this->belongsTo('App\Post');
  }
}

/**
* Many to Many
*/
class User extends Model
{
    public function roles()
    {
      return $this->belogsToMany('App\Role');
    }
}

$roles = App\User::find(1)->roles()->orderBy('name')->get();


// Define the inverse of the Relationship

class Role extends Model
{
  public function users()
  {
    return $this->belongsToMany('App\User');
  }
}

/**
* Has Many Through
*/

/**
*countries
*    id - integer
*    name - string

*users
*    id - integer
*    country_id - integer
*    name - string

*posts
*    id - integer
*    user_id - integer
*    title - string
*/

class Country extends Model
{
  public function posts()
  {
    // the first parameter is the final model we wish to access
    return $this->hasManyThrough('App\Post', 'App\User');
  }

  public function posts()
    {
        return $this->hasManyThrough(
            'App\Post',
            'App\User',
            'country_id', // Foreign key on users table...
            'user_id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'id' // Local key on users table...
        );
    }
}
