<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
  protected $fillable = [
    'name', 'description', 'hours', 'days', 'company_id', 'user_id', 'project_id'
  ];

  public function companies()
  {
    $this->belongsToMany('App\Company');
  }

  public function projects()
  {
    $this->belongsToMany('App\Project');
  }

  public function user()
  {
    $this->belongsToMany('App\User');
  }
}
