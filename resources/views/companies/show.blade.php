@extends('layouts/app')

@section('content')
<div class=container>
  <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
      <h1 class="display-4 font-italic">{{$company->name}}</h1>
      <p class="lead my-3">{{$company->description}}</p>
      <p class="lead mb-0"><a href="#" class="text-white font-weight-bold">Continue reading...</a></p>
    </div>
  </div>
</div>
<main role="main" class='container'>
  <div class="row">
    <div class='col-md-8 blog-main'>
      @foreach($projects as $project)
        <div class="col-md-8 col-lrg-8 pull-left">
          <h2>Heading</h2>
          <p>{{$project->name}}</p>
          <p><a class="btn btn-secondary" href="#" role="button">View details »</a></p>
        </div>
      @endforeach
    </div>
    <aside class="col-md-4 blog-sidebar">
      <!-- <div class="p-3 mb-3 bg-light rounded">
        <h4 class="font-italic">About</h4>
        <p class="mb-0">Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
      </div> -->

      <div class="p-3">
        <h4 class="font-italic">Actions</h4>
        <ol class="list-unstyled mb-0">
          <li><a href="/companies/{{$company->id}}/edit">Edit</a></li>
          <li>
            <a href="#" onclick="
                  var result = confirm('Are you sure you wish to delete this Company?');
                      if( result ){
                              event.preventDefault();
                              document.getElementById('delete-form').submit();
                      }
                          "
            >Delete
            </a>
            <!-- The form for deleting the company -->
            <form id="delete-form" action="{{ route('companies.destroy', [$company->id]) }}"
              method="POST" style="display: none;">
                <input type="hidden" name="_method" value="delete">
                {{ csrf_field() }}
            </form>
          </li>
          <li><a href="{{ route('companies.create') }}">Create New Company</a></li>
        </ol>
      </div>

      <div class="p-3">
        <h4 class="font-italic">Elsewhere</h4>
        <ol class="list-unstyled">
          <li><a href="#">GitHub</a></li>
          <li><a href="#">Twitter</a></li>
          <li><a href="#">Facebook</a></li>
        </ol>
      </div>
    </aside><!-- /.blog-sidebar -->
  </div>
  <hr>
</main>

@endsection
